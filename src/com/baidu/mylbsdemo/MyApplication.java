package com.baidu.mylbsdemo;

import java.util.ArrayList;
import java.util.List;

import android.app.Application;
import android.util.Log;

import com.baidu.mylbsdemo.bean.Info;

public class MyApplication extends Application {
	private List<Info> infoList;//历史标注存储
	

	public MyApplication() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		infoList = new ArrayList<Info>();
	}
	
	
	public List<Info> getList() {
		return infoList;
	}
	
	public void setList(List<Info> list) {
		this.infoList = list;
	}
	

}
