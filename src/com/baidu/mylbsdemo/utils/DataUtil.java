package com.baidu.mylbsdemo.utils;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class DataUtil {
	private static List<String> numbers2;
	
	public static List<String> getData() {
		// test data
		numbers2 = new ArrayList<String>();
		for (int i = 0; i < 30; i++) {
			numbers2.add("100000" + i);
		}
		return numbers2;
	}
	
	
    /**
     * 判断是否是WIFI连接
     * 
     * @param context
     * @return true means connected, false means not connected
     */
    public static boolean isWifiConnected(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            NetworkInfo info = connectivityManager.getActiveNetworkInfo();
            if (info != null) {
                if (info.getType() == ConnectivityManager.TYPE_WIFI) {
                    return info.isAvailable();
                }
            }
        }
        return false;
    }
    
     
    /**
     * 判断网络是否能用
     * @param context
     * @return
     */
    public static boolean isNetworkAvailable(Context context) {
    	ConnectivityManager mManager = (ConnectivityManager) context   
                .getSystemService(Context.CONNECTIVITY_SERVICE); 
    	NetworkInfo info = mManager.getActiveNetworkInfo(); 
    	  if (info != null && info.isAvailable()){ 
    	       //能联网 
    	        return true; 
    	  }else{
    	       //不能联网 
    	        return false; 
    	  }
    }
}
