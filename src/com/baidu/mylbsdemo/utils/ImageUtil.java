package com.baidu.mylbsdemo.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.CursorLoader;
import android.content.res.Resources.NotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.util.Log;

public class ImageUtil {
	private static final float RATIO_THRES = 1.2f;
	private static final int BITMAP_QUALITY = 85;
	private static final String tag = "ImageUtility";
	public final static int BUFFER_SIZE = 8 * 1024;

	public static BitmapFactory.Options getOptions(Context context, Uri imageUri) {
		if (context == null || imageUri == null) {
			return null;
		}
		BitmapFactory.Options options = new BitmapFactory.Options();
		try {
			options.inJustDecodeBounds = true;
			InputStream is = context.getContentResolver().openInputStream(
					imageUri);
			BitmapFactory.decodeStream(is, null, options);
			is.close();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return options;
	}

	public static float getCompressRatio(int originalWidth, int originalHeight,
			int maxSideLength) {
		float Ratio = 1;
		if (originalWidth > maxSideLength || originalHeight > maxSideLength) {
			if (originalWidth > originalHeight) {
				Ratio = (float) maxSideLength / (float) originalWidth;
			} else {
				Ratio = (float) maxSideLength / (float) originalHeight;
			}
		}
		return Ratio;
	}

	/**
	 * compress image by the max side's length, and save the image to filePath
	 * 
	 * @param inputStream
	 *            Image inputStream
	 * @param maxSideLength
	 * @param filePath
	 * @return true means success, false means fail
	 */
	public static boolean compressImage(Context context, Uri imageUri,
			int maxSideLength, String filePath) {
		if (context == null || imageUri == null || maxSideLength <= 0
				|| filePath == null) {
			return false;
		}

		try {
			BitmapFactory.Options options = getOptions(context, imageUri);
			int bitmapWidth = options.outWidth;
			int bitmapHeight = options.outHeight;
			System.out.println("bitmap size is: " + bitmapWidth + " "
					+ bitmapHeight);

			options.inJustDecodeBounds = false;
			options.inSampleSize = calculateInSampleSize(options,
					maxSideLength, maxSideLength);
			InputStream is = context.getContentResolver().openInputStream(
					imageUri);
			Bitmap originalBitmap = BitmapFactory.decodeStream(is, null,
					options);
			is.close();

			bitmapWidth = originalBitmap.getWidth();
			bitmapHeight = originalBitmap.getHeight();
			Matrix matrix = new Matrix();
			float ratio = getCompressRatio(bitmapWidth, bitmapHeight,
					maxSideLength);
			matrix.postScale(ratio, ratio);
			Bitmap bitmap2 = Bitmap.createBitmap(originalBitmap, 0, 0,
					bitmapWidth, bitmapHeight, matrix, true);

			// Bitmap resizeBitmap = grayBitmap(bitmap2);
			if (!originalBitmap.isRecycled()) {
				originalBitmap.recycle();
			}
			/*
			 * if (!bitmap2.isRecycled()) { bitmap2.recycle(); }
			 */

			// saved in file
			File file = new File(filePath);
			FileOutputStream out = null;
			try {
				out = new FileOutputStream(file);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			/*
			 * resizeBitmap.compress(Bitmap.CompressFormat.JPEG, BITMAP_QUALITY,
			 * out);
			 */
			bitmap2.compress(Bitmap.CompressFormat.JPEG, BITMAP_QUALITY, out);
			try {
				out.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}

			try {
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

			/*
			 * if (!resizeBitmap.isRecycled()) { resizeBitmap.recycle(); }
			 */
			if (!bitmap2.isRecycled()) {
				bitmap2.recycle();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return true;
	}

	public static boolean compressImage(Context context, String imagePath,
			int maxSideLength, String filePath) {
		if (context == null || imagePath == null || maxSideLength <= 0
				|| filePath == null) {
			return false;
		}

		boolean result = true;
		try {
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			Bitmap bitmap = BitmapFactory.decodeFile(imagePath, options);
			int bitmapWidth = options.outWidth;
			int bitmapHeight = options.outHeight;

			Log.d(tag, "oright width is " + bitmapWidth + " height is "
					+ bitmapHeight);
			float widthHeigthRatio = getCompressRatio(bitmapWidth,
					bitmapHeight, maxSideLength);

			Log.d(tag, "widthHeigthRatio is " + widthHeigthRatio);
			options.inJustDecodeBounds = false;
			bitmap = BitmapFactory.decodeFile(imagePath, options);
			Matrix matrix = new Matrix();
			matrix.postScale(widthHeigthRatio, widthHeigthRatio);
			Bitmap bitmap2 = Bitmap.createBitmap(bitmap, 0, 0, bitmapWidth,
					bitmapHeight, matrix, true);
			Bitmap resizeBitmap = grayBitmap(bitmap2);
			Log.d(tag, "resizebitmap size is: " + resizeBitmap.getWidth() + " "
					+ resizeBitmap.getHeight());
			if (!bitmap.isRecycled()) {
				bitmap.recycle();
			}
			if (!bitmap2.isRecycled()) {
				bitmap2.recycle();
			}

			// saved in file
			File file = new File(filePath);
			FileOutputStream out = null;
			try {
				out = new FileOutputStream(file);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				result = false;
			}
			resizeBitmap.compress(Bitmap.CompressFormat.JPEG, BITMAP_QUALITY,
					out);
			try {
				out.flush();
			} catch (IOException e) {
				e.printStackTrace();
				result = false;
			}

			try {
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
				result = false;
			}

			if (!resizeBitmap.isRecycled()) {
				resizeBitmap.recycle();
			}
		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		}

		return result;
	}

	/**
	 * gray bitmap
	 * 
	 * @param bitmap
	 *            original bitmap
	 * @return grayed bitmap
	 */
	public static Bitmap grayBitmap(Bitmap bitmap) {
		if (bitmap == null) {
			return null;
		}
		try {
			int width = bitmap.getWidth();
			int height = bitmap.getHeight();

			Log.d(tag, "gray size is " + width + " " + height);
			Bitmap grayBmp = Bitmap.createBitmap(width, height,
					Bitmap.Config.RGB_565);
			Canvas canvas = new Canvas(grayBmp);
			Paint paint = new Paint();
			ColorMatrix colorMatrix = new ColorMatrix();
			colorMatrix.setSaturation(0);
			ColorMatrixColorFilter filter = new ColorMatrixColorFilter(
					colorMatrix);
			paint.setColorFilter(filter);

			canvas.drawBitmap(bitmap, 0, 0, paint);
			return grayBmp;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * make Bitmap to new scale
	 * 
	 * @param bm
	 *            origin bitmap
	 * @param width
	 *            new bitmap width
	 * @param height
	 *            new bitmap height
	 * @return the new scale bitmap
	 */
	public static Bitmap scaleBitmap(Bitmap bm, int width, int height) {
		if (bm == null || width <= 0 || height <= 0) {
			return null;
		}

		int originWidth = bm.getWidth();
		int originHeight = bm.getHeight();

		float scaleWidth = ((float) width) / originWidth;
		float scaleHeight = ((float) height) / originHeight;

		Matrix matrix = new Matrix();
		matrix.postScale(scaleWidth, scaleHeight);
		Bitmap newBm = Bitmap.createBitmap(bm, 0, 0, width, height, matrix,
				true);

		return newBm;
	}

	public static boolean scaleBitmap(Bitmap bm, int width, int height,
			String filePath) {
		if (bm == null || width <= 0 || height <= 0 || filePath == null) {
			return false;
		}

		int originWidth = bm.getWidth();
		int originHeight = bm.getHeight();

		float scaleWidth = ((float) width) / (float) originWidth;
		float scaleHeight = ((float) height) / (float) originHeight;

		Log.d(tag, "scale width is " + width + " height is " + height);
		Log.d(tag, "scale originWidth is " + originWidth + " originHeight is "
				+ originHeight);
		Log.d(tag, "scaleWidth ratio is " + scaleWidth + " scaleHeight is "
				+ scaleHeight);
		Matrix matrix = new Matrix();
		matrix.postScale(scaleWidth, scaleHeight);
		Bitmap newBm = Bitmap.createBitmap(bm, 0, 0, originWidth, originHeight,
				matrix, true);

		boolean result = false;
		// saved in file
		File file = new File(filePath);
		FileOutputStream out = null;
		try {
			out = new FileOutputStream(file);
			if (newBm.compress(Bitmap.CompressFormat.JPEG, BITMAP_QUALITY, out)) {
				out.flush();
				out.close();
			}
			result = true;
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
					result = false;
				}
			}
		}
		return result;
	}

	public static Bitmap scaleBitmap(Context context, int imageId, int width,
			int height) {
		if (context == null || width < 0 || height < 0) {
			return null;
		}

		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeResource(context.getResources(), imageId, options);
		int originWidth = options.outWidth;
		int originHeight = options.outHeight;

		if (width == 0) {
			width = originWidth;
		}
		if (height == 0) {
			height = originHeight;
		}

		Log.d("utility", "height is " + originHeight);
		float scaleWidth = ((float) width) / originWidth;
		float scaleHeight = ((float) height) / originHeight;

		Matrix matrix = new Matrix();
		matrix.postScale(scaleWidth, scaleHeight);

		options.inJustDecodeBounds = false;
		Bitmap bm = BitmapFactory.decodeResource(context.getResources(),
				imageId, options);
		Bitmap newBm = Bitmap.createBitmap(bm, 0, 0, originWidth, originHeight,
				matrix, true);

		return newBm;
	}

	public static int dipToPx(Context context, float dip) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (dip * scale + 0.5f);
	}

	public static int pxToDip(Context context, float px) {
		final float scale = context.getResources().getDisplayMetrics().density;
		return (int) (px / scale + 0.5f);
	}

	/**
	 * read image by min memory
	 * 
	 * @param context
	 * @param resId
	 * @return
	 */
	public static Bitmap readBitmap(Context context, int resId) {
		BitmapFactory.Options opt = new BitmapFactory.Options();
		opt.inPreferredConfig = Bitmap.Config.RGB_565;
		opt.inPurgeable = true;
		opt.inInputShareable = true;
		// get image resource
		InputStream is = null;
		try {
			context.getResources().openRawResource(resId);
		} catch (NotFoundException e) {
			e.printStackTrace();
			return null;
		}
		return BitmapFactory.decodeStream(is, null, opt);
	}

	public static boolean saveBitmapToFile(Bitmap bitmap, String filePath) {
		try {
			FileOutputStream out = null;
			try {
				out = new FileOutputStream(filePath);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				return false;
			}
			return bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public static Bitmap getMaxSideBitmap(InputStream inputStream, int ratio) {
		BitmapFactory.Options opts = new BitmapFactory.Options();
		opts.inSampleSize = ratio;
		opts.inJustDecodeBounds = false;
		return BitmapFactory.decodeStream(inputStream, null, opts);
	}

	/**
	 * get compress ratio
	 * 
	 * @param inputStream
	 * @param maxWidth
	 * @param maxHeight
	 * @return
	 */
	public static int getRatio(InputStream inputStream, int maxWidth,
			int maxHeight) {
		if (inputStream == null || maxWidth <= 0 || maxHeight <= 0) {
			return 1;
		}

		BitmapFactory.Options options = new BitmapFactory.Options();
		// indicate just decode the file header but not whole content
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeStream(inputStream, null, options);

		int ratio = 1;
		float widthRatio = (float) maxWidth / (float) options.outWidth;
		float heightRatio = (float) maxHeight / (float) options.outHeight;
		Log.d("bitmap", "outWidth is " + options.outWidth + " outHeight is "
				+ options.outHeight);
		if (widthRatio < heightRatio) {
			ratio = (int) widthRatio;
		} else {
			ratio = (int) heightRatio;
		}

		Log.d("bitmap", "ratio is " + ratio);
		return ratio;
	}

	/**
	 * get rotate degree from uri
	 * 
	 * @param context
	 * @param contentUri
	 * @return rotate degree
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public static int getRotateFromUri(Context context, Uri contentUri) {
		String scheme = contentUri.getScheme();
		if (context == null || scheme == null || scheme.equals("")) {
			return 0;
		}

		int degree = 0;
		if (scheme.equals("content")) {
			String[] proj = { MediaStore.Images.Media.ORIENTATION };
			CursorLoader loader = new CursorLoader(context, contentUri, proj,
					null, null, null);
			Cursor cursor = loader.loadInBackground();
			if (cursor == null) {
				return 0;
			}
			try {
				int column_index = cursor
						.getColumnIndexOrThrow(MediaStore.Images.Media.ORIENTATION);

				cursor.moveToFirst();
				degree = cursor.getInt(column_index);
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} finally {
				if (cursor != null) {
					cursor.close();
				}
			}
		} else if (scheme.equals("file")) {
			Log.d(tag, "scheme is file");
			try {
				String fileName = contentUri.getPath();
				Log.d(tag, "fileName is " + fileName);
				if (isStringEmpty(fileName)) {
					ExifInterface exif = new ExifInterface(fileName);
					int orientation = exif.getAttributeInt(
							ExifInterface.TAG_ORIENTATION, -1);
					if (orientation != -1) {
						// We only recognize a subset of orientation tag values.
						switch (orientation) {
						case ExifInterface.ORIENTATION_ROTATE_90:
							degree = 90;
							break;
						case ExifInterface.ORIENTATION_ROTATE_180:
							degree = 180;
							break;
						case ExifInterface.ORIENTATION_ROTATE_270:
							degree = 270;
							break;
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		Log.d(tag, "degress is " + degree);
		return degree;
	}

	public static Bitmap getBitmapFromUriWithOrientation(Context context,
			Uri imageUri, int reqWidth, int reqHeight) {
		if (imageUri == null || reqWidth < 0 || reqHeight < 0) {
			return null;
		}

		Log.d(tag, "imageUri is " + imageUri);
		int rotate = getRotateFromUri(context, imageUri);
		Log.d(tag, "cursor is " + rotate);

		return getBitmapFromUri(context, imageUri, reqWidth, reqHeight, rotate);
	}

	public static Bitmap rotate(Bitmap bmp, int width, int height, int rotate) {
		try {
			Matrix m = new Matrix();
			m.postRotate(rotate);
			Bitmap b2 = Bitmap.createBitmap(bmp, 0, 0, width, height, m, true);

			if (bmp != b2) {
				bmp.recycle();
				bmp = b2;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bmp;
	}

	public static Bitmap getBitmapFromUri(Context context, Uri imageUri,
			int reqWidth, int reqHeight, int rotate) {
		if (context == null || imageUri == null || reqWidth < 0
				|| reqHeight < 0) {
			return null;
		}
		Log.d(tag, "orientation is " + rotate);
		Log.d(tag, "uri is " + imageUri);
		InputStream is;
		try {
			try {
				is = context.getContentResolver().openInputStream(imageUri);
			} catch (NotFoundException e) {
				e.printStackTrace();
				return null;
			}

			BitmapFactory.Options options = new BitmapFactory.Options();
			// First decode with inJustDecodeBounds=true to check dimensions
			// indicate just decode the file header but not whole content
			options.inJustDecodeBounds = true;
			try {
				BitmapFactory.decodeStream(is, null, options);
			} catch (IllegalArgumentException ex) {
				ex.printStackTrace();
				return null;
			}

			is.close();
			Log.d(tag, "before bitmap time is " + System.currentTimeMillis());
			if (options.outHeight <= 0 || options.outWidth <= 0) {
				return null;
			}

			options.inSampleSize = calculateInSampleSize(options, reqWidth,
					reqHeight);
			Log.d(tag,
					"after calculateInSampleSize time is "
							+ System.currentTimeMillis());
			// Decode bitmap with inSampleSize set
			options.inJustDecodeBounds = false;
			try {
				is = context.getContentResolver().openInputStream(imageUri);
			} catch (NotFoundException e) {
				e.printStackTrace();
				return null;
			}

			Bitmap originBitmap = BitmapFactory.decodeStream(is, null, options);
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}

			// 非图片格式文件
			if (null == originBitmap) {
				return null;
			}

			Bitmap bitmap = rotate(originBitmap, options.outWidth,
					options.outHeight, rotate);
			return bitmap;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static Bitmap getBitmapFromPath(Context context, String imagePath,
			int reqWidth, int reqHeight, int rotate) {
		if (context == null || imagePath == null || reqWidth < 0
				|| reqHeight < 0) {
			return null;
		}
		Log.d(tag, "orientation is " + rotate);
		Log.d(tag, "imagePath is " + imagePath);
		InputStream is;
		File file = new File(imagePath);
		if (file == null || !file.exists()) {
			return null;
		}
		try {
			try {
				is = new FileInputStream(file);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				return null;
			}

			BitmapFactory.Options options = new BitmapFactory.Options();
			// First decode with inJustDecodeBounds=true to check dimensions
			// indicate just decode the file header but not whole content
			options.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(is, null, options);
			is.close();
			Log.d(tag, "before bitmap time is " + System.currentTimeMillis());
			options.inSampleSize = calculateInSampleSize(options, reqWidth,
					reqHeight);
			Log.d(tag,
					"after calculateInSampleSize time is "
							+ System.currentTimeMillis());
			// Decode bitmap with inSampleSize set
			options.inJustDecodeBounds = false;
			try {
				is = new FileInputStream(file);
			} catch (NotFoundException e) {
				e.printStackTrace();
				return null;
			}

			Bitmap originBitmap = BitmapFactory.decodeStream(is, null, options);
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}

			Bitmap bitmap = rotate(originBitmap, options.outWidth,
					options.outHeight, rotate);
			return bitmap;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/***
	 * get sample ratio according to given required width and height - step 2
	 * 
	 * @param options
	 *            - containing the raw width and height parameter
	 * @param reqWidth
	 *            - required width , normally much less than raw width, e.g.
	 *            want to show a 1920 *1080 in a 200 * 160 ImageView
	 * @param reqHeight
	 *            - required height
	 * @return sample ratio
	 */
	public static int calculateInSampleSize(BitmapFactory.Options options,
			int reqWidth, int reqHeight) {
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		Log.d(tag, "old width = " + width + ", old height = " + height);
		Log.d(tag, "new width = " + reqWidth + ", new height = " + reqHeight);
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {
			int inSampleSize_width = Math.round((float) height
					/ (float) reqHeight);
			int inSampleSize_height = Math.round((float) width
					/ (float) reqWidth);
			Log.d(tag, String.format(
					"the inSamplewidth  is %d and sampleheight is %d",
					inSampleSize_width, inSampleSize_height));
			inSampleSize = inSampleSize_width > inSampleSize_height ? inSampleSize_width
					: inSampleSize_height;
			inSampleSize = inSampleSize > 1 ? inSampleSize : 1;
			// Using powers of 2 for inSampleSize values is faster and more
			// efficient for the decoder
			int power = 1;
			for (; power <= inSampleSize; power *= 2)
				;
			float ratio = inSampleSize / (float) (power / 2);
			if (ratio < RATIO_THRES) {
				inSampleSize = power / 2;
			}

		}
		Log.d(tag, String.format("the inSample size is %d", inSampleSize));
		return inSampleSize;
	}

	/**
	 * save bitmap from uri to file
	 * 
	 * @param context
	 * @param imageUri
	 * @param filePath
	 * @return true means success, false means failed
	 */
	public static boolean saveBitmap(Context context, Uri imageUri,
			String filePath) {
		if (context == null || imageUri == null || filePath == null) {
			return false;
		}

		try {
			InputStream is = context.getContentResolver().openInputStream(
					imageUri);
			is = context.getContentResolver().openInputStream(imageUri);
			// saved in file
			FileOutputStream fos = new FileOutputStream(filePath);
			int size = 0;
			byte[] buffer = new byte[BUFFER_SIZE];
			while ((size = is.read(buffer)) != -1) {
				fos.write(buffer, 0, size);
				fos.flush();
			}
			is.close();
			fos.close();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	/**
	 * Judge whether the string is null
	 * 
	 * @param str
	 * @return true means the string is not null，false means null
	 */
	public static boolean isStringEmpty(String str) {
		if (str == null || str.equals("")) {
			return false;
		}
		return true;
	}

	/**
	 * 检查uri是否是图片文件
	 * 
	 * @param context
	 * @param imageUri
	 * @return
	 */
	public static boolean checkBitmapFormat(Context context, Uri imageUri) {
		if (context == null || imageUri == null) {
			return false;
		}
		InputStream is;
		try {
			is = context.getContentResolver().openInputStream(imageUri);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		}

		BitmapFactory.Options options = new BitmapFactory.Options();
		// First decode with inJustDecodeBounds=true to check dimensions
		// indicate just decode the file header but not whole content
		options.inJustDecodeBounds = true;
		try {
			BitmapFactory.decodeStream(is, null, options);
		} catch (IllegalArgumentException ex) {
			ex.printStackTrace();
			return false;
		}

		try {
			is.close();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}

		if (options.outHeight <= 0 || options.outWidth <= 0) {
			return false;
		}

		return true;
	}

	public static byte[] Bitmap2Bytes(Bitmap bm) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
		return baos.toByteArray();
	}

	/**
	 * 根据路径获取图片
	 * 
	 * @param pathString
	 * @return
	 */
	public static Bitmap getDiskBitmap(String pathString) {
		Bitmap bitmap = null;
		try {
			Log.i("zdg", "ImageUtil----getDiskBitmap---pathString="+pathString);
			File file = new File(pathString);
			if (file.exists()) {
				bitmap = BitmapFactory.decodeFile(pathString);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		return bitmap;
	}

	/**
	 * 压缩图片
	 * @param path
	 * @param w
	 * @param h
	 * @return
	 */
	public static Bitmap convertToBitmap(String path, int w, int h) {
		getDiskBitmap(path);
		Log.i("zdg", "ImageUtil----convertToBitmap---path="+path);
		BitmapFactory.Options opts = new BitmapFactory.Options();
		// 设置为ture只获取图片大小
		opts.inJustDecodeBounds = true;
		opts.inPreferredConfig = Bitmap.Config.ARGB_8888;
		// 返回为空
		BitmapFactory.decodeFile(path, opts);
		int width = opts.outWidth;
		int height = opts.outHeight;
		float scaleWidth = 0.f, scaleHeight = 0.f;
		if (width > w || height > h) {
			// 缩放
			scaleWidth = ((float) width) / w;
			scaleHeight = ((float) height) / h;
		}
		opts.inJustDecodeBounds = false;
		float scale = Math.max(scaleWidth, scaleHeight);
		opts.inSampleSize = (int) scale;
//		WeakReference<Bitmap> weak = new WeakReference<Bitmap>(
//				BitmapFactory.decodeFile(path, opts));
		Bitmap bitmap = BitmapFactory.decodeFile(path, opts);
		return Bitmap.createScaledBitmap(bitmap, w, h, true);
	}
	
	private static Bitmap compressImage(Bitmap image) {    
	    
        ByteArrayOutputStream baos = new ByteArrayOutputStream();    
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos);//质量压缩方法，这里100表示不压缩，把压缩后的数据存放到baos中    
        int options = 90;    
        while ( baos.toByteArray().length / 1024>100) {  //循环判断如果压缩后图片是否大于100kb,大于继续压缩           
            baos.reset();//重置baos即清空baos    
            image.compress(Bitmap.CompressFormat.JPEG, options, baos);//这里压缩options%，把压缩后的数据存放到baos中    
            options -= 10;//每次都减少10    
        }    
        ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());//把压缩后的数据baos存放到ByteArrayInputStream中    
        Bitmap bitmap = BitmapFactory.decodeStream(isBm, null, null);//把ByteArrayInputStream数据生成图片    
        return bitmap;    
    } 
	
	public static Bitmap getimage(String srcPath) {    
        BitmapFactory.Options newOpts = new BitmapFactory.Options();    
        //开始读入图片，此时把options.inJustDecodeBounds 设回true了    
        newOpts.inJustDecodeBounds = true;    
        Bitmap bitmap = BitmapFactory.decodeFile(srcPath,newOpts);//此时返回bm为空    
            
        newOpts.inJustDecodeBounds = false;    
        int w = newOpts.outWidth;    
        int h = newOpts.outHeight;    
        //现在主流手机比较多是800*480分辨率，所以高和宽我们设置为    
        float hh = 800f;//这里设置高度为800f    
        float ww = 480f;//这里设置宽度为480f    
        //缩放比。由于是固定比例缩放，只用高或者宽其中一个数据进行计算即可    
        int be = 1;//be=1表示不缩放    
        if (w > h && w > ww) {//如果宽度大的话根据宽度固定大小缩放    
            be = (int) (newOpts.outWidth / ww);    
        } else if (w < h && h > hh) {//如果高度高的话根据宽度固定大小缩放    
            be = (int) (newOpts.outHeight / hh);    
        }    
        if (be <= 0)    
            be = 1;    
        newOpts.inSampleSize = be;//设置缩放比例    
        //重新读入图片，注意此时已经把options.inJustDecodeBounds 设回false了    
        bitmap = BitmapFactory.decodeFile(srcPath, newOpts);   
        Log.i("zdg", "getimage------bitmap="+bitmap);
        return bitmap;
//        return compressImage(bitmap);//压缩好比例大小后再进行质量压缩    
    } 
	
}
