package com.baidu.mylbsdemo.viewpager;

import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class MyViewPagerAdapter extends PagerAdapter {
	private List<Bitmap> mListViews;
	private Context mContext;

	public MyViewPagerAdapter(Context context, List<Bitmap> mListViews) {
		mContext = context.getApplicationContext();
		this.mListViews = mListViews;
	}

	@Override
	public int getCount() {
		return mListViews.size();
	}
	
	@Override
	public float getPageWidth(int position) {
		return (float) 0.5;
	}

	/**
	 * 关联key 与 obj是否相等，即是否为同一个对象
	 */
	@Override
	public boolean isViewFromObject(View view, Object obj) {
		return view == obj; 
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView((View) object); // 将view 类型 的object容器中移除,根据key
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		ImageView iv = new ImageView(mContext);
		iv.setImageBitmap(mListViews.get(position));
		((ViewPager) container).addView(iv, position);
		return iv;//
	}
}
