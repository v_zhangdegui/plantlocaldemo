package com.baidu.mylbsdemo.bean;

import java.io.Serializable;

/**
 * 用于储存历史标注数据
 * @author Administrator
 *
 */
public class Info implements Serializable
{
	private static final long serialVersionUID = -758459502806858414L;
	/**
	 * 经度
	 */
	private double latitude;
	/**
	 * 纬度
	 */
	private double longitude;
	/**
	 * 图片路径
	 */
	private String path;

	/**
	 * 名称
	 */
	private String name;


	public Info()
	{
	}

	public Info(String name,double latitude, double longitude, String path)
	{
		super();
		this.latitude = latitude;
		this.longitude = longitude;
		this.path = path;
		this.name = name;
	}

	public double getLatitude()
	{
		return latitude;
	}

	public void setLatitude(double latitude)
	{
		this.latitude = latitude;
	}

	public double getLongitude()
	{
		return longitude;
	}

	public void setLongitude(double longitude)
	{
		this.longitude = longitude;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}
	
	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
}