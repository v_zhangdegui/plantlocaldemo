package com.baidu.mylbsdemo;

import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BaiduMap.OnMapClickListener;
import com.baidu.mapapi.map.BaiduMap.OnMarkerClickListener;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.InfoWindow;
import com.baidu.mapapi.map.InfoWindow.OnInfoWindowClickListener;
import com.baidu.mapapi.map.MapPoi;
import com.baidu.mapapi.map.MapStatusUpdate;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.MyLocationConfigeration;
import com.baidu.mapapi.map.MyLocationConfigeration.LocationMode;
import com.baidu.mapapi.map.MyLocationData;
import com.baidu.mapapi.map.OverlayOptions;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mylbsdemo.MyOrientationListener.OnOrientationListener;
import com.baidu.mylbsdemo.bean.Info;
import com.baidu.mylbsdemo.utils.DataUtil;
import com.baidu.mylbsdemo.viewpager.MyViewPagerAdapter;

public class MainActivity extends Activity {
	private static final int RESULT_BACK_NAME = 11;
	private static long PrepKeyDownTime = 0;
	private static final int DOUBLE_BACK_INTERVAL = 1500;//双击退出应用
	/**
	 * 地图控件
	 */
	private MapView mMapView = null;
	/**
	 * 地图实例
	 */
	private BaiduMap mBaiduMap;
	/**
	 * 
	 * 定位的客户端
	 */
	private LocationClient mLocationClient;

	/**
	 * 当前定位的模式
	 */
	private LocationMode mCurrentMode = LocationMode.NORMAL;

	/**
	 * 覆盖物
	 */
	private Marker marker;
	/***
	 * 是否是第一次定位
	 */
	private volatile boolean isFristLocation = true;
	/**
	 * 最新一次的经纬度
	 */
	private double mCurrentLantitude;
	private double mCurrentLongitude;

	/**
	 * 均值
	 */
	private double mAverLantitude = 0;
	private double mAverLongitude = 0;
	/**
	 * 当前的精度
	 */
	private float mCurrentAccracy;
	/**
	 * 方向传感器的监听器
	 */
	private MyOrientationListener myOrientationListener;
	/**
	 * 方向传感器X方向的值
	 */
	private int mXDirection;
	/**
	 * 地图定位的模式
	 */
	private String[] mStyles = new String[] { "地图模式【正常】", "地图模式【跟随】",
			"地图模式【罗盘】" };
	private int mCurrentStyle = 0;
	
	// 初始化全局 bitmap
	private BitmapDescriptor mIconMaker;
	/**
	 * 详细信息的 布局
	 */
	private RelativeLayout mViewPagerContainer;
	/**
	 * 返回的结果
	 */
	private String mPlantName;

	/**
	 * 经纬度处理
	 */
	private Double[] latitudes;
	private Double[] longitudes;
	private int count = 2;
	private int i = 0;

	private MyLocationListener mMyLocationListener;
	private List<Bitmap> listMaps;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		// 注意该方法要再setContentView方法之前实现
		SDKInitializer.initialize(getApplicationContext());
		setContentView(R.layout.activity_main);
		// 第一次定位
		isFristLocation = true;
		// 获取地图控件引用
		mMapView = (MapView) findViewById(R.id.id_bmapView);
		mViewPagerContainer = (RelativeLayout) findViewById(R.id.id_marker_info);
		// 获得地图的实例
		mBaiduMap = mMapView.getMap();
		mIconMaker = BitmapDescriptorFactory.fromResource(R.drawable.maker);
		// MapStatusUpdate msu = MapStatusUpdateFactory.zoomTo(7.0f);
		// mBaiduMap.setMapStatus(msu);
		// 设置地图最大以及最小缩放级别，地图支持的最大最小级别分别为[3-19]
		mBaiduMap.setMaxAndMinZoomLevel(3, 19);
		// 初始化定位
		initMyLocation();
		// 初始化传感器
		initOritationListener();
		initMapClickEvent();
		latitudes = new Double[3];
		longitudes = new Double[3];
	}

	private void initMapClickEvent() {
		mBaiduMap.setOnMapClickListener(new OnMapClickListener() {

			@Override
			public boolean onMapPoiClick(MapPoi arg0) {
				return false;
			}

			@Override
			public void onMapClick(LatLng arg0) {
				mViewPagerContainer.setVisibility(View.GONE);
				mBaiduMap.hideInfoWindow();
			}
		});
	}

	/**
	 * 覆盖物的点击处理
	 */
	private void initMarkerClickEvent() {
		// 对Marker的点击
		mBaiduMap.setOnMarkerClickListener(new OnMarkerClickListener() {
			@Override
			public boolean onMarkerClick(final Marker marker) {
				// 获得marker中的数据
				Info info = (Info) marker.getExtraInfo().get("info");

				InfoWindow mInfoWindow;
				// 生成一个TextView用户在地图中显示InfoWindow
				TextView location = new TextView(getApplicationContext());
				location.setBackgroundResource(R.drawable.location_tips);
				location.setPadding(30, 20, 30, 50);
				location.setText(info.getName());
				// 将marker所在的经纬度的信息转化成屏幕上的坐标
				final LatLng ll = marker.getPosition();
				Point p = mBaiduMap.getProjection().toScreenLocation(ll);
				p.y -= 47;
				LatLng llInfo = mBaiduMap.getProjection().fromScreenLocation(p);
				// 为弹出的InfoWindow添加点击事件
				mInfoWindow = new InfoWindow(location, llInfo,
						new OnInfoWindowClickListener() {

							@Override
							public void onInfoWindowClick() {
								// 隐藏InfoWindow
								mBaiduMap.hideInfoWindow();
							}
						});
				// 显示InfoWindow
				mBaiduMap.showInfoWindow(mInfoWindow);
				// 设置详细信息布局为可见
				mViewPagerContainer.setVisibility(View.VISIBLE);
				// 根据商家信息为详细信息布局设置信息
				popupInfo(mViewPagerContainer, info);
				return true;
			}
		});
	}

	/**
	 * 历史标注展示
	 * @param container
	 * @param info
	 */
	protected void popupInfo(RelativeLayout container, Info info) {
		final ViewPager viewPager = (ViewPager) container
				.findViewById(R.id.my_viewpager);
		MyViewPagerAdapter myViewPagerAdapter = new MyViewPagerAdapter(
				getApplicationContext(), listMaps);
		viewPager.setAdapter(myViewPagerAdapter);
		// 1.设置幕后item的缓存数目
		viewPager.setOffscreenPageLimit(3);
		// 2.设置页与页之间的间距
		viewPager.setPageMargin(1);
		// 3.将父类的touch事件分发至viewPgaer，否则只能滑动中间的一个view对象
		container.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				return viewPager.dispatchTouchEvent(event);
			}
		});

		viewPager.setOnPageChangeListener(new OnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				if (viewPager != null) {
//					 viewPager.invalidate();
					//TODO 实现点击放大
				}
			}

			@Override
			public void onPageScrolled(int position, float positionOffset,
					int positionOffsetPixels) {

			}

			@Override
			public void onPageScrollStateChanged(int state) {

			}
		});
	}


	/**
	 * 初始化方向传感器
	 */
	private void initOritationListener() {
		myOrientationListener = new MyOrientationListener(
				getApplicationContext());
		myOrientationListener
				.setOnOrientationListener(new OnOrientationListener() {
					@Override
					public void onOrientationChanged(float x) {
						mXDirection = (int) x;
						// 构造定位数据
						MyLocationData locData = new MyLocationData.Builder()
								.accuracy(mCurrentAccracy)
								// 此处设置开发者获取到的方向信息，顺时针0-360
								.direction(mXDirection)
								.latitude(mCurrentLantitude)
								.longitude(mCurrentLongitude).build();
						// 设置定位数据
						mBaiduMap.setMyLocationData(locData);
						// 设置自定义图标
						BitmapDescriptor mCurrentMarker = BitmapDescriptorFactory
								.fromResource(R.drawable.navi_map_gps_locked);
						MyLocationConfigeration config = new MyLocationConfigeration(
								mCurrentMode, true, mCurrentMarker);
						mBaiduMap.setMyLocationConfigeration(config);

					}
				});
	}

	/**
	 * 初始化定位
	 */
	private void initMyLocation() {
		// 定位初始化
		mLocationClient = new LocationClient(getApplicationContext());
		// 设置定位的相关配置
		/**
		 * 设置定位参数包括：定位模式（高精度定位模式，低功耗定位模式和仅用设备定位模式），返回坐标类型，是否打开GPS等等。
		 * 高精度定位模式：这种定位模式下，会同时使用网络定位和GPS定位，优先返回最高精度的定位结果；
		 * 低功耗定位模式：这种定位模式下，不会使用GPS，只会使用网络定位（Wi-Fi和基站定位）
		 * 仅用设备定位模式：这种定位模式下，不需要连接网络，只使用GPS进行定位，这种模式下不支持室内环境的定位
		 */
		LocationClientOption option = new LocationClientOption();
		mMyLocationListener = new MyLocationListener();
		mLocationClient.registerLocationListener(mMyLocationListener);
		if (DataUtil.isNetworkAvailable(getApplicationContext())) {
			option.setLocationMode(LocationClientOption.LocationMode.Hight_Accuracy);
		} else {
			option.setLocationMode(LocationClientOption.LocationMode.Device_Sensors);
		}
		// option.setLocationMode(LocationClientOption.LocationMode.Battery_Saving);
		option.setOpenGps(true);// 打开gps
		option.setCoorType("bd09ll"); // 设置坐标类型
		option.setScanSpan(1000);
		mLocationClient.setLocOption(option);
	}

	/**
	 * 初始化图层
	 * 
	 * @param list
	 */
	public void addInfosOverlay(List<Info> list) {
		mBaiduMap.clear();
		LatLng latLng = null;
		OverlayOptions overlayOptions = null;
		Marker marker = null;
		listMaps = new ArrayList<Bitmap>();
		for (Info info : list) {
			File file = new File(info.getPath());
			Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
			listMaps.add(bitmap);
			// 位置
			latLng = new LatLng(info.getLatitude(), info.getLongitude());
			// 图标
			overlayOptions = new MarkerOptions().position(latLng)
					.icon(mIconMaker).zIndex(1);
			marker = (Marker) (mBaiduMap.addOverlay(overlayOptions));
			Bundle bundle = new Bundle();
			bundle.putSerializable("info", info);
			marker.setExtraInfo(bundle);
		}
		// 将地图移到到最后一个经纬度位置
		MapStatusUpdate u = MapStatusUpdateFactory.newLatLng(latLng);
		mBaiduMap.setMapStatus(u);
		/**
		 * OverlayOptions options = new MarkerOptions() .position(llA)
		 * //设置marker的位置 .icon(bdA) //设置marker图标 .zIndex(9) //设置marker所在层级
		 * .draggable(true); //设置手势拖拽 //将marker添加到地图上 marker = (Marker)
		 * (mBaiduMap.addOverlay(options));
		 */
	}

	/**
	 * 初始化我的位置图层 
	 */
	public void addMyPlaceOverlay(final double latitude, final double longitude) {
		mBaiduMap.clear();
		// 位置
		LatLng latLng = new LatLng(latitude, longitude);
		// 设置 marker 覆盖物的坐标 图标 覆盖物的 Index
		OverlayOptions overlayOptions = new MarkerOptions().position(latLng)
				.icon(mIconMaker).zIndex(1);
		marker = (Marker) (mBaiduMap.addOverlay(overlayOptions));
		mBaiduMap.setOnMarkerClickListener(new OnMarkerClickListener() {
			@Override
			public boolean onMarkerClick(Marker arg0) {
				Intent intent = new Intent(getApplicationContext(),
						PopupActivity.class);
				Bundle bundler = new Bundle();
				if (mAverLantitude != 0 && mAverLongitude != 0) {
					bundler.putDouble("latitude", mAverLantitude);
					bundler.putDouble("longitude", mAverLongitude);
				}
				intent.putExtras(bundler);
				startActivityForResult(intent, RESULT_BACK_NAME);
				return true;
			}
		});
		MapStatusUpdate u = MapStatusUpdateFactory.newLatLng(latLng);
		mBaiduMap.setMapStatus(u);
	}

	/**
	 * 获取PopupActivity返回的名称
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == RESULT_BACK_NAME && data != null) {
			Bundle extras = data.getExtras();
			mPlantName = extras.getString("name");
		}
		// 清空地图所有的 Overlay 覆盖物以及 InfoWindow
		mBaiduMap.clear();
	}


	/**
	 * 实现实位回调监听
	 */
	public class MyLocationListener implements BDLocationListener {
		@Override
		public void onReceiveLocation(BDLocation location) {
			// map view 销毁后不在处理新接收的位置
			if (location == null || mMapView == null)
				return;

			// 求均值
			latitudes[i] = location.getLatitude();
			longitudes[i] = location.getLongitude();
			if ((latitudes[i] != 0 || longitudes[i] != 0) && count <= 3) {
				if (i == 2) {
					latitudes[i] = location.getLatitude();
					longitudes[i] = location.getLongitude();
					i = 0;
					mAverLantitude = getAverageLatitude();
					mAverLongitude = getAverageLongitude();
				} else {
					latitudes[++i] = location.getLatitude();
					longitudes[i] = location.getLongitude();
				}
			}
			// 构造定位数据
			MyLocationData locData = new MyLocationData.Builder()
					.accuracy(location.getRadius())
					// 此处设置开发者获取到的方向信息，顺时针0-360
					.direction(mXDirection).latitude(location.getLatitude())
					.longitude(location.getLongitude()).build();
			mCurrentAccracy = location.getRadius();
			// 设置定位数据，那个圈
			mBaiduMap.setMyLocationData(locData);
			
			mCurrentLantitude = location.getLatitude();
			mCurrentLongitude = location.getLongitude();
			
			// 设置自定义图标
			BitmapDescriptor mCurrentMarker = BitmapDescriptorFactory
					.fromResource(R.drawable.navi_map_gps_locked);
			MyLocationConfigeration config = new MyLocationConfigeration(
					mCurrentMode, true, mCurrentMarker);
			// MyLocationConfigeration config = new MyLocationConfigeration(
			// mCurrentMode, true, null);
			mBaiduMap.setMyLocationConfigeration(config);
			// 第一次定位时，将地图位置移动到当前位置
			if (isFristLocation) {
				isFristLocation = false;
				LatLng ll = new LatLng(location.getLatitude(),
						location.getLongitude());
				MapStatusUpdate u = MapStatusUpdateFactory.newLatLng(ll);
				mBaiduMap.animateMapStatus(u);
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu, menu);
		return super.onCreateOptionsMenu(menu);
	}

	/**
	 * 默认点击menu菜单，菜单项不显示图标，反射强制其显示
	 */
	@Override
	public boolean onMenuOpened(int featureId, Menu menu) {

		if (featureId == Window.FEATURE_OPTIONS_PANEL && menu != null) {
			if (menu.getClass().getSimpleName().equals("MenuBuilder")) {
				try {
					Method m = menu.getClass().getDeclaredMethod(
							"setOptionalIconsVisible", Boolean.TYPE);
					m.setAccessible(true);
					m.invoke(menu, true);
				} catch (Exception e) {
				}
			}

		}
		return super.onMenuOpened(featureId, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.id_menu_map_common:// 普通地图
			mViewPagerContainer.setVisibility(View.GONE);
			mBaiduMap.setMapType(BaiduMap.MAP_TYPE_NORMAL);
			break;
		case R.id.id_menu_map_site:// 卫星地图
			mViewPagerContainer.setVisibility(View.GONE);
			mBaiduMap.setMapType(BaiduMap.MAP_TYPE_SATELLITE);
			break;
		case R.id.id_menu_map_traffic:// 开启交通图
			mViewPagerContainer.setVisibility(View.GONE);
			if (mBaiduMap.isTrafficEnabled()) {
				item.setTitle("开启实时交通");
				mBaiduMap.setTrafficEnabled(false);
			} else {
				item.setTitle("关闭实时交通");
				mBaiduMap.setTrafficEnabled(true);
			}
			break;
		case R.id.id_menu_map_myLoc:// 我的位置
			mViewPagerContainer.setVisibility(View.GONE);
			center2myLoc();
			if (mAverLantitude != 0 && mAverLongitude != 0) {
				addMyPlaceOverlay(mCurrentLantitude, mCurrentLongitude);
			} else {
				Toast.makeText(getApplicationContext(), "当前网络较差，请稍后重试",
						Toast.LENGTH_LONG).show();
			}
			break;
		case R.id.id_menu_map_addMaker:// 查看标注的植物
			MyApplication application = (MyApplication) getApplication();
			List<Info> list = application.getList();
			Log.i("zdg", "id_menu_map_addMaker-----list.size="+list.size());
			if (list.size() <= 0) {//如果list集合为空，说明还没有标注过
				Toast.makeText(getApplicationContext(), "您还没有添加标注，请先标注",
						Toast.LENGTH_LONG).show();
			} else {
				mViewPagerContainer.setVisibility(View.GONE);
				addInfosOverlay(list);
				initMarkerClickEvent();
			}
			break;
		case R.id.id_menu_map_style:// 设置自定义图标
			mViewPagerContainer.setVisibility(View.GONE);
			mCurrentStyle = (++mCurrentStyle) % mStyles.length;
			item.setTitle(mStyles[mCurrentStyle]);
			switch (mCurrentStyle) {
			case 0:
				mCurrentMode = LocationMode.NORMAL;
				break;
			case 1:
				mCurrentMode = LocationMode.FOLLOWING;
				break;
			case 2:
				mCurrentMode = LocationMode.COMPASS;
				break;
			}
			BitmapDescriptor mCurrentMarker = BitmapDescriptorFactory
					.fromResource(R.drawable.navi_map_gps_locked);
			MyLocationConfigeration config = new MyLocationConfigeration(
					mCurrentMode, true, mCurrentMarker);
			mBaiduMap.setMyLocationConfigeration(config);
			break;
		}

		return super.onOptionsItemSelected(item);
	}

	/**
	 *坐标处理，获取3次的值，求 均值
	 * @return
	 */
	private double getAverageLatitude() {
		double mAveLantitude1 = 0;
		if (latitudes.length > 0) {
			for (int i = 0; i < latitudes.length; i++) {
				mAveLantitude1 += latitudes[i];
			}
		}
		return mAveLantitude1 / 3;
	}

	private double getAverageLongitude() {
		double mAveLongitude1 = 0;
		if (longitudes.length > 0) {
			for (int i = 0; i < longitudes.length; i++) {
				mAveLongitude1 += longitudes[i];
			}
		}
		return mAveLongitude1 / 3;
	}

	/**
	 * 地图移动到我的位置, 直接拿最近一次经纬度.
	 */
	private void center2myLoc() {
		LatLng ll = new LatLng(mCurrentLantitude, mCurrentLongitude);
		MapStatusUpdate u = MapStatusUpdateFactory.newLatLng(ll);
		mBaiduMap.animateMapStatus(u);
	}

	@Override
	protected void onStart() {
		// 开启图层定位
		mBaiduMap.setMyLocationEnabled(true);
		if (!mLocationClient.isStarted()) {
			mLocationClient.start();
		}
		// 开启方向传感器
		myOrientationListener.start();
		super.onStart();
	}

	@Override
	protected void onResume() {
		super.onResume();
		mMapView.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
		mMapView.onPause();
	}

	
	@Override
	protected void onStop() {
		super.onStop();
		// 关闭图层定位
		mBaiduMap.setMyLocationEnabled(false);
		mLocationClient.stop();
		// 关闭方向传感器
		myOrientationListener.stop();
	}

	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		mIconMaker.recycle();
		mMapView.onDestroy();
		mLocationClient.unRegisterLocationListener(mMyLocationListener);
	}

	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if ((System.currentTimeMillis() - PrepKeyDownTime) < DOUBLE_BACK_INTERVAL) {
				finish();
				return true;
			}
			PrepKeyDownTime = System.currentTimeMillis();
			Toast.makeText(this, "再按一次，退出应用", Toast.LENGTH_SHORT).show();
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

}