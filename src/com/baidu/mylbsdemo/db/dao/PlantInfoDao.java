/**
 * 
 */
package com.baidu.mylbsdemo.db.dao;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;

import com.baidu.mylbsdemo.db.PlantInforHelper;

/**
 * @author Administrator
 * 
 */
public class PlantInfoDao {
	private PlantInforHelper helper;

	public PlantInfoDao(Context context) {
		helper = new PlantInforHelper(context);
	}

	/**
	 * 添加信息 直接存储图片
	 * 
	 * @param phone
	 * @param mode
	 */
	public void insertAllInfoBitmap(String name, double latitude,
			double longitude, Bitmap bmp) {
		SQLiteDatabase db = helper.getWritableDatabase();
		// 第二步，声明并创建一个输出字节流对象
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		// 第三步，调用compress将Bitmap对象压缩为PNG格式，第二个参数为PNG图片质量，第三个参数为接收容器，即输出字节流os
		bmp.compress(Bitmap.CompressFormat.PNG, 100, os);
		// 第四步，将输出字节流转换为字节数组，并直接进行存储数据库操作，注意，所对应的列的数据类型应该是BLOB类型
		ContentValues values = new ContentValues();
		values.put("name", name);
		values.put("latitude", latitude);
		values.put("longitude", longitude);

		values.put("photo", os.toByteArray());
		db.insert("plantinfors", null, values);
		db.close();
	}

	/**
	 * 添加信息 图片以路径存储
	 * 
	 * @param phone
	 * @param mode
	 */
	public void insertAllInfo(String name, double latitude, double longitude,
			String path) {
		SQLiteDatabase db = helper.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("name", name);
		values.put("latitude", latitude);
		values.put("longitude", longitude);

		values.put("photo", path);
		db.insert("plantinfors", null, values);
		db.close();
	}

	/**
	 * 添加经度和纬度
	 * 
	 * @param phone
	 * @param mode
	 */
	public void insertInfo(double latitude, double longitude, String tag) {
		SQLiteDatabase db = helper.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("latitude", latitude);
		values.put("longitude", longitude);
		db.insert("plantinfors", null, values);
		db.close();
	}


	/**
	 * 插入图片
	 * 
	 * @param phone
	 * @param mode
	 */
	public void insertPhoto(byte[] bim, String tag) {
		SQLiteDatabase db = helper.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("bitmap", bim);
		values.put("tag", tag);
		db.insert("plantinfors", null, values);
		db.close();
	}

	/**
	 * 查询姓名
	 * 
	 * @param groupPosition
	 * @return
	 */
	public String queryName(double latitude, double longitude) {
		// select name from classlist where idx='1'
		SQLiteDatabase db = helper.getReadableDatabase();
		String name = null;
		Cursor cursor = db.query(
				"plantinfors",
				new String[] { "name" },
				"latitude=? and longitude =?",
				new String[] { String.valueOf(latitude),
						String.valueOf(longitude) }, null, null, null);
		while (cursor.moveToNext()) {
			name = cursor.getString(cursor.getColumnIndex("name"));
			cursor.close();
			db.close();
		}
		return name;
	}

	/**
	 * 根据经纬度，获取图片
	 * 
	 * @param groupPosition
	 * @return
	 */
	public Drawable queryImage(double latitude, double longitude) {

		SQLiteDatabase db = helper.getReadableDatabase();
		byte[] in = null;
		BitmapDrawable bd = null;
		Cursor cr = db
				.rawQuery(
						"select photo from plantinfors where latitude = ? and longitude = ?",
						new String[] { String.valueOf(latitude),
								String.valueOf(longitude) });

		while (cr.moveToNext()) {

			in = cr.getBlob(cr.getColumnIndex("photo"));

		}
		if (in.length > 0) {
			Bitmap bmp = BitmapFactory.decodeByteArray(in, 0, in.length);
			bd = new BitmapDrawable(bmp);
		}
		cr.close();
		db.close();
		return bd;
	}

	
	
	
	/**
	 * 根据植物名，查询经纬度
	 * 
	 * @param name
	 * @return
	 */
	public List<Map<String, Double>> queryName(String name) {
		SQLiteDatabase db = helper.getWritableDatabase();
		double mlatitude = 0;
		double mlongitude =0;
		List<Map<String, Double>> list = new ArrayList<Map<String,Double>>();
		Map<String , Double> map = new HashMap<String, Double>();
		Cursor cur = db
				.rawQuery(
						"select latitude,longitude from plantinfors where name = ?",
						new String[] { name });

		while (cur.moveToNext()) {
			mlatitude = cur.getDouble(cur.getColumnIndex("latitude"));
			map.put("latitude", mlatitude);
			list.add(map);
			mlongitude = cur.getDouble(cur.getColumnIndex("longitude"));
			map.put("longitude", mlongitude);
			list.add(map);
		}
		cur.close();
		db.close();
		return list;
	}

	
	/**
	 * 根据植物名，查询图片
	 * @param plantName
	 * @return
	 */
	public List<String> queryDrawable(String plantName) {
		SQLiteDatabase db = helper.getWritableDatabase();
		List<String> mapList = new ArrayList<String>();
		Cursor cur = db
				.rawQuery(
						"select photo from plantinfors where name = ?",
						new String[] { plantName });

		while (cur.moveToNext()) {
			String photo = cur.getString(cur.getColumnIndex("photo"));
			mapList.add(photo);
		}
		cur.close();
		db.close();
		return mapList;
	}
	
	
	/**
	 * 根据植物名、经纬度，获取id
	 * 
	 * @param groupPosition
	 * @return
	 */
	public int queryId(String name , double latitude ,double longitude) {

		SQLiteDatabase db = helper.getReadableDatabase();
		int id = 0;
		Cursor cur = db
				.rawQuery(
						"select _id from plantinfors where name = ? and latitude = ? and longitude = ?",
						new String[] {name ,String.valueOf(latitude),String.valueOf(longitude)});

		while (cur.moveToNext()) {
			id = cur.getInt(cur.getColumnIndex("_id"));
		}
		cur.close();
		db.close();
		return id;
	}
	
	/**
	 *  图片以路径存储
	 * 
	 * @param phone
	 * @param mode
	 */
	public void insertPhoto(Integer id,String path) {
		SQLiteDatabase db = helper.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put("plant_id", id);
		values.put("path", path);
		db.insert("photoContainer", null, values);
		db.close();
	}

	
	/**
	 * 往另一个表中插入数据
	 * @param mapInfo
	 */
	public void insertPhoto(Map<String, String> mapInfo) {
		SQLiteDatabase db = helper.getWritableDatabase();
		ContentValues values = new ContentValues();
		Iterator<Map.Entry<String, String>> it = mapInfo
				.entrySet().iterator();
		for (Map.Entry<String, String> entry : mapInfo
				.entrySet()) {
			values.put(entry.getKey(), entry.getValue());
			db.insert("photoContainer", null, values);
			it.next();
		}
		db.close();
	}
	
}
