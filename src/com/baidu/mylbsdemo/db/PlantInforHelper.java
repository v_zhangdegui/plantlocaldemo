package com.baidu.mylbsdemo.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class PlantInforHelper extends SQLiteOpenHelper {

	private static final int VERSION = 1;

	String msql_plantinfors = "create table if not exists plantinfors"
			+ "(_id integer primary key,name varchar,latitude double,longitude double,description varchar,photo String,String tag)";

	String sal_photo = "create table if not exists photoContainer"
			+ "(_id integer primary key,plant_id integer , path String ,String tag)";

	public PlantInforHelper(Context context) {
		super(context, "plantinfors.db", null, VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
//		 db.execSQL("DROP TABLE IF EXISTS plantinfors");
//		 db.execSQL("DROP TABLE IF EXISTS photoContainer");
		db.execSQL(msql_plantinfors);
		db.execSQL(sal_photo);
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
	}

}
