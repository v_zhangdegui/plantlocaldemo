package com.baidu.mylbsdemo;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.app.Activity;
import android.app.Application;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.baidu.mylbsdemo.bean.Info;
import com.baidu.mylbsdemo.db.dao.PlantInfoDao;
import com.baidu.mylbsdemo.utils.DataUtil;
import com.baidu.mylbsdemo.viewpager.MyViewPagerAdapter;

public class PopupActivity extends Activity implements OnClickListener,
		OnItemClickListener {
	private static final int BEGIN_CAPTURE_1 = 20;
	private EditText etName;
	private List<String> numbers;
	private PopupWindow popupWindow;
	private Bitmap bitmap2;// 要存储的图片
	private String plantName;
	private double latitude; // 要存储的经度
	private double longitude; // 要存储的纬度
	private String path;
	private boolean isFromItem = false;
	private ViewPager viewPager;
	private LinearLayout container;
	private List<Bitmap> bitmapList;
	private Button bt_take_camera;
	private boolean goOnTakeCamera = false;
	private PlantInfoDao dao;
	private TextView tv_total;
	private MyApplication application;
	private List<Info> list;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		application = (MyApplication) getApplication();
		list = application.getList();
		Intent intent = this.getIntent();
		if (intent != null) {
			Bundle extras = intent.getExtras();
			latitude = extras.getDouble("latitude");
			longitude = extras.getDouble("longitude");
		}
		dao = new PlantInfoDao(getApplicationContext());
		bitmapList = new ArrayList<Bitmap>();
		setContentView(R.layout.popup_layout);
		initView();
	}

	private void initView() {
		Button bt_ok = (Button) findViewById(R.id.bt_ok);
		Button bt_no = (Button) findViewById(R.id.bt_no);
		bt_take_camera = (Button) findViewById(R.id.bt_take_camera);
		if (!goOnTakeCamera) {
			bt_take_camera.setText("拍照");
		}
		etName = (EditText) findViewById(R.id.et_name);
		ImageButton buttonArrow = (ImageButton) findViewById(R.id.ib_down_arrow);
		buttonArrow.setOnClickListener(this);
		bt_ok.setOnClickListener(this);
		bt_no.setOnClickListener(this);
		bt_take_camera.setOnClickListener(this);

		container = (LinearLayout) findViewById(R.id.image_container);
		tv_total = (TextView) findViewById(R.id.tv_total);
		if (bitmapList.size() == 0) {
			tv_total.setText("总的拍摄了\n0张图片");
		} else {
			tv_total.setText("总的拍摄了\n" + bitmapList.size() + "张图片");
		}
		viewPager = (ViewPager) findViewById(R.id.viewpayger);
	}

	private void initViewPager() {
		Log.i("zdg", "initViewPager-----come in--");
		viewPager.setVisibility(View.VISIBLE);
		MyViewPagerAdapter myViewPagerAdapter = new MyViewPagerAdapter(
				getApplicationContext(), bitmapList);
		viewPager.setAdapter(myViewPagerAdapter);
		// 1.设置幕后item的缓存数目
		viewPager.setOffscreenPageLimit(3);
		// 2.设置页与页之间的间距
		viewPager.setPageMargin(1);
		// 3.将父类的touch事件分发至viewPgaer，否则只能滑动中间的一个view对象
		container.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				return viewPager.dispatchTouchEvent(event);
			}
		});

		viewPager.setOnPageChangeListener(new OnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				if (viewPager != null) {
					// viewPager.invalidate();
				}
			}

			@Override
			public void onPageScrolled(int position, float positionOffset,
					int positionOffsetPixels) {

			}

			@Override
			public void onPageScrollStateChanged(int state) {

			}
		});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.bt_ok:// 确定
			if (latitude != 0 && longitude != 0 && plantName != null
					&& path != null) {
				saveData(plantName, latitude, longitude, path);
			}
			Bundle mBundler = new Bundle();
			mBundler.putString("name", plantName);
			Intent result = new Intent();
			result.putExtras(mBundler);
			setResult(RESULT_OK, result);
			finish();
			break;
		case R.id.bt_no:// 取消
			finish();
			break;
		case R.id.bt_take_camera:// 拍照
			if (etName.getText() != null && !isFromItem) {
				plantName = etName.getText().toString();
				DataUtil.getData().add(etName.getText().toString());
			}

			if (bt_take_camera.getText().equals("继续拍照")) {
				goOnTakeCamera = true;
			}
			Intent intent = new Intent();
			intent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);
			intent.addCategory(Intent.CATEGORY_DEFAULT);
			startActivityForResult(intent, BEGIN_CAPTURE_1);
			break;
		case R.id.ib_down_arrow:// 下拉框
			showSelectNumberDialog();
			break;

		default:
			break;
		}
	}

	/**
	 * 点击确定后，保存数据
	 */
	private void saveData(final String plantName, final double latitude,
			final double longitude, final String path) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				// 保存最近一次拍的结果
				Info info = new Info();
				info.setName(plantName);
				info.setLatitude(latitude);
				info.setLongitude(longitude);
				info.setPath(path);
				list.add(info);// 添加到全局集合变量
				saveAllData(plantName, latitude, longitude, path);// 将数据存储到数据库
				// 保存数据到另一张表
				savePhotoTODb(plantName, latitude, longitude);
			}
		}).start();
	}

	/**
	 * 存储数据到数据库
	 * 
	 * @param longitude2
	 * @param latitude2
	 * @param plantName2
	 * @param bytes2
	 */
	private void saveAllData(final String plantName, final double latitude,
			final double longitude, final String path) {
		dao.insertAllInfo(plantName, latitude, longitude, path);
	}

	/**
	 * 存储图片到另一个表
	 * 
	 * @param longitude2
	 * @param latitude2
	 * @param plantName2
	 * @param bytes2
	 */
	private void savePhotoTODb(final String plantName, final double latitude,
			final double longitude) {
		int myId = dao.queryId(plantName, latitude, longitude);
		dao.insertPhoto(Integer.valueOf(myId), path);
	}

	/**
	 * 拍完照后，返回照片的处理
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == BEGIN_CAPTURE_1 && data != null) {
			try {
				ContentResolver resolver = getContentResolver();
				Uri originalUri = data.getData(); // 获得图片的uri
				if (originalUri != null) {
					bitmap2 = MediaStore.Images.Media.getBitmap(resolver,
							originalUri); // 显得到bitmap图片
				} else {
					Bundle bundle = data.getExtras();
					if (bundle != null) {
						bitmap2 = (Bitmap) bundle.get("data");
					}
				}
				bitmapList.add(bitmap2);
				initViewPager();// 初始化viewPager
				// 改变textView状态
				tv_total.setText("总的拍摄了" + bitmapList.size() + "张图片");
				bt_take_camera.setText("继续拍照");
				saveImageInSDCard(bitmap2);
				if (goOnTakeCamera) {
					// 如果选择了继续拍照，则将前一次的拍照结果保存到数据库
					saveData(plantName, latitude, longitude, path);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void saveImageInSDCard(final Bitmap bitmap2) {
		try {
			path = getSavePath();
			File file = new File(path);
			FileOutputStream out = new FileOutputStream(file);
			bitmap2.compress(Bitmap.CompressFormat.JPEG, 100, out);
			out.flush();
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	/**
	 * 以时间戳给图片命名,避免重复
	 * 
	 * @return
	 */
	private String getSavePath() {
		File file = new File(Environment.getExternalStorageDirectory() + "/");
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
		Date date = new Date();
		String params = format.format(date);
		String filePath = Environment.getExternalStorageDirectory() + "/"
				+ params + ".jpg";
		return filePath;
	}

	/**
	 * 下拉选择框，popupwindow实现
	 */
	private void showSelectNumberDialog() {
		numbers = DataUtil.getData();

		ListView lv = new ListView(this);
		lv.setBackgroundResource(R.drawable.listview_background);
		lv.setOnItemClickListener(this);

		NumbersAdapter adapter = new NumbersAdapter();
		lv.setAdapter(adapter);

		popupWindow = new PopupWindow(lv, etName.getWidth() - 4, 320);
		// 设置popupWindow可点击
		popupWindow.setOutsideTouchable(true);
		popupWindow.setBackgroundDrawable(new BitmapDrawable());

		popupWindow.setFocusable(true);
		// 微调
		popupWindow.showAsDropDown(etName, 2, -5);

	}

	class NumbersAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			return numbers.size();
		}

		@Override
		public Object getItem(int position) {
			return numbers.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			NumberViewHolder mHolder = null;
			if (convertView == null) {
				mHolder = new NumberViewHolder();
				convertView = LayoutInflater.from(getApplicationContext())
						.inflate(R.layout.numbers_item, null);
				mHolder.tvNumber = (TextView) convertView
						.findViewById(R.id.tv_number);
				convertView.setTag(mHolder);
			} else {
				mHolder = (NumberViewHolder) convertView.getTag();
			}

			mHolder.tvNumber.setText(numbers.get(position));
			mHolder.tvNumber.setTag(position);
			return convertView;
		}

	}

	public static class NumberViewHolder {
		public TextView tvNumber;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {

		plantName = numbers.get(position);
		etName.setText(plantName);
		isFromItem = true;
		popupWindow.dismiss();
	}

	@Override
	protected void onResume() {
		super.onResume();
		// 隐藏软件盘
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

}
